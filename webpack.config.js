var webpack = require('webpack'),
	path = require('path'),
	DIST_DIR = path.resolve(__dirname, 'dist'),
	SRC_DIR = path.resolve(__dirname, 'src'),
	ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: path.join(SRC_DIR, '/app/index.js'),
	output: {
		path: path.join(DIST_DIR, '/app'),
		filename: 'bundle.js',
		publicPath: '/app/'
	},
	devtool: 'inline-source-map',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/node_modules/],
				use: [{
					loader: 'babel-loader'
				}]
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use: "css-loader"
				})
			},
			{
				test: /\.(png|jpg|svg|ttf)$/,
				use: "file-loader?name=[name].[ext]"
			}
		]
	},
	plugins: [
		new ExtractTextPlugin('bundle.css')
	]
};