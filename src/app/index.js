import React from 'react';
import ReactDOM from 'react-dom';
import App from './javascript/app';
import './styles/styles.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import reducer from './javascript/reducers/reducer';
import thunk from 'redux-thunk';

const store = createStore(reducer, applyMiddleware(thunk, createLogger()));

ReactDOM.render(<Provider store = { store }><App/></Provider>, document.getElementById('app'));