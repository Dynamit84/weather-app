const MONTHS = {
    '01': 'Jan.',
    '02': 'Feb.',
    '03': 'Mar.',
    '04': 'Apr.',
    '05': 'May.',
    '06': 'Jun.',
    '07': 'Jul.',
    '08': 'Aug.',
    '09': 'Sep.',
    '10': 'Oct.',
    '11': 'Nov.',
    '12': 'Dec.'
};

export const weatherFetchHasErrored = error => {
	
	return {
		type: 'FETCH_WEATHER_REJECTED',
		payload: error
	};
};

export const weatherDataIsLoading = bool => {
	
	return {
		type: 'FETCH_WEATHER_PENDING',
		payload: bool
	};
};

export const weatherFetchSuccess = data => {
	
	return {
		type: 'FETCH_WEATHER_FULFILLED',
		payload: data
	};
};

export const changeActiveChart = activeChart => {

    return {
        type: 'CHANGE_ACTIVE_CHART',
        payload: activeChart
    }
};

export const searchCitiesResult = response => {

    return {
        type: 'FETCH_CITIES_FULFILLED',
        payload: response
    }
};

export const emptySearchResult = () => {

    return {
        type: 'EMPTY_SEARCH_RESULT'
    }
};

export const fetchWeather = coords => dispatch => {

	dispatch(weatherDataIsLoading(true));

	getWeatherData(coords)
	.then(data => {
		dispatch(weatherDataIsLoading(false));
		
		return data;
	})
	.then(data => {
		dispatch(weatherFetchSuccess(data));
	})
	.catch(() => {
        dispatch(weatherDataIsLoading(false));
		dispatch(weatherFetchHasErrored('unable to retrieve data from the server'));
    });
};

export const getCurrentPosition = () => dispatch => {

	navigator.geolocation.getCurrentPosition(
		(position) => {
			const lat = position.coords.latitude,
				lon = position.coords.longitude;

            dispatch(fetchWeather({ lat, lon }));
		},
		() => {
			dispatch(weatherFetchHasErrored('unable to retrieve your location'));
            dispatch(weatherDataIsLoading(false));
		});
};

let getWeatherData = coords => {
	let { lat, lon } = coords;
	
	return fetch(`http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&units=metric&appid=e81e98410b55421503581ee78e25d080`)
		.then(response => {
			if (response.ok) {

				return Promise.resolve(response);
			} else {

				return Promise.reject();
			}
		})
		.then(response => response.json())
		.then(data => extractData(data))
		.catch(() => {throw (new Error())});
};

let extractData = apiData => {
	let { list, city } = apiData,
		temperatureArr = [],
		pressureArr = [],
		humidityArr = [],
		speedArr = [],
		dateObj = {
			date: [],
			time: []
		},
		descriptionArr = [],
		iconArr = [];
	
	list.slice(0,20).forEach(data => {
		let { temp, pressure, humidity } = data.main,
			{ speed } = data.wind,
			singleDate = formatDate(data.dt_txt),
			{ description, icon } = data.weather[0];
		
		temperatureArr.push(temp);
		pressureArr.push(pressure);
		humidityArr.push(humidity);
		speedArr.push(speed);
        dateObj.date.push(singleDate[0]);
        dateObj.time.push(singleDate[1]);
		descriptionArr.push(description);
		iconArr.push(icon);
	});
	
	return {
		cityName: city.name,
		country: city.country,
		weatherData: {
			temperature: temperatureArr,
			pressure: pressureArr,
			humidity: humidityArr,
			speed: speedArr,
			date: dateObj,
			description: descriptionArr,
			icon: iconArr
		}
	}
};

let formatDate = (fetchedDate) => {
	let splitedDate = fetchedDate.split(' '),
		time = splitedDate[1].split(':').splice(0,2),
		date = splitedDate[0].split('-').splice(1,2);

	return concatDate(time, date);
};

let concatDate = (time, date) => {
	let formattedDate = [];

    formattedDate.push(`${date[1]} ${MONTHS[date[0]]}`);
    formattedDate.push(time.join(':'));

	return formattedDate;
};