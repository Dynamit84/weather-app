import Highcharts from 'highcharts';

export const drawChart = chartData => {
    let {
    	cityName,
		country,
		elemId,
		weatherData,
		date,
		dataName,
		chartColor,
		dataUnit,
		tickInterval
    } = chartData;

    Highcharts.chart( elemId, {

        legend: {
            align: 'right',
            verticalAlign: 'top',
            x: -30,
            y: 30,
            floating: true
        },

        chart: {
            zoomType: 'x',
			type: 'spline',
			backgroundColor: '#e2e2e2'
        },

        title: {
            text: `Weather and forecasts in ${cityName}, ${country}`
        },
        yAxis: {
            title: {
                text: ''
            },
            tickInterval: tickInterval,
            labels: {
                style: {
                    color: chartColor
                },
                format: `{value} ${dataUnit}`
            }
        },
        xAxis: {
            type: 'datetime',
            categories: date.time
        },

        tooltip: {
            borderColor: '#CDCDCD',
            backgroundColor: {
                linearGradient: [0, 0, 0, 60],
                stops: [
                    [0, '#FFFFFF'],
                    [1, '#E0E0E0']
                ]
            },
            formatter() {

                return `<span>${date.date[this.point.index]} ${date.time[this.point.index]}</span><br><span style="color: ${chartColor};">${dataName}: </span><b>${this.y.toFixed(1)}</b> ${dataUnit}`;
            }
        },

        series: [{
            name: dataName,
            color: chartColor,
            data: weatherData
        }]
    });
};