import React from 'react';
import * as actions from '../actions/actions';
import { connect } from 'react-redux';

class SearchResult extends React.Component {

    fetchCityWeather(coords) {
        this.props.fetchWeather(coords);
        this.props.emptySearchResult();
    }

    render() {
        if(this.props.searchResult === null) {

            return null;
        }

        if(this.props.searchResult.length === 0) {

            return <ul className='result'>
                <li className="resultItem">We couldn't find such city</li>
            </ul>;
        }

        return <ul className='result'>
            {
                this.props.searchResult.map((city, index) => {
                        const { name, sys, coord } = city;

                        return <li key={index} className="resultItem" onClick={this.fetchCityWeather.bind(this, coord)}>
                                 <div className="content-wrapper">
                                     <span>{name}, {sys.country}</span>
                                     <span>Geo coords [{coord['lat'].toFixed(2)}, {coord['lon'].toFixed(2)}]</span>
                                 </div>
                               </li>
                }
                )
            }
        </ul>
    }
}

export default connect(null, actions)(SearchResult);