import React, { Component } from 'react';
import { drawChart } from '../utils/drawChart';
import * as actions from '../actions/actions';
import { connect } from 'react-redux';
import { Tabs } from './Tabs';


class Charts extends Component {

    constructor(props) {
        super(props);

        this.chartData = [];
    }

    collectChartsData() {

        const { cityName, country } = this.props.data;
        const { date, temperature, pressure, speed, humidity } = this.props.data.weatherData;

        if(!date) {
            return;
        }

        this.chartData = [
            {
                cityName,
                country,
                elemId: 'temperatureChart',
                dataName: 'Temperature',
                chartColor: '#ff0000',
                weatherData: temperature,
                dataUnit: '°C',
                tickInterval: 5,
                date
            },
            {
                cityName,
                country,
                elemId: 'windChart',
                dataName: 'Wind',
                chartColor: '#808080',
                weatherData: speed,
                dataUnit: 'm/s',
                tickInterval: 1,
                date
            },
            {
                cityName,
                country,
                elemId: 'pressureChart',
                dataName: 'Pressure',
                chartColor: '#008000',
                weatherData: pressure,
                dataUnit: 'hpa',
                tickInterval: 5,
                date
            },
            {
                cityName,
                country,
                elemId: 'humidityChart',
                dataName: 'Humidity',
                chartColor: '#1360f3',
                weatherData: humidity,
                dataUnit: '%',
                tickInterval: 10,
                date
            }
        ];

    }

    drawCharts() {

        this.chartData.forEach(singleData => drawChart(singleData));

    }

    componentDidMount() {

        this.drawCharts();

    }

    componentDidUpdate() {

        this.drawCharts();

    }

    render() {
        const { activeChart, changeActiveChart } = this.props;

        this.collectChartsData();

        return (
        	<div>
				<Tabs data = { this.chartData } activeTab = { activeChart } onTabClick = { changeActiveChart }/>
				<div className="weather-charts">
					{
                        this.chartData.map((elem, index) => {

						    const active = activeChart === elem.dataName.toLowerCase() ? 'active' : '';

							return <div key={index} id={elem.elemId} className={`chart ${active}`}></div>
						})
					}
				</div>
			</div>
        );
    }
}

function mapStateToProps(state) {

    return {
        activeChart: state.activeChart
    };
}

export default connect(mapStateToProps, actions)(Charts);
