import React from 'react';

const DAYS = {
		0: 'Sunday',
		1: 'Monday',
		2: 'Tuesday',
		3: 'Wednesday',
		4: 'Thursday',
		5: 'Friday',
		6: 'Saturday'
	},
	MONTHS = {
		0: 'January',
		1: 'February',
		2: 'March',
		3: 'April',
		4: 'May',
		5: 'June',
		6: 'July',
		7: 'August',
		8: 'September',
		9: 'October',
		10: 'November',
		11: 'December'
	};

export const GeneralInfo = (props) => {

	if(!props.data.country) {

		return null;
	}
	
	let { temperature, date, icon } = props.data.weatherData,
		{ cityName, country } = props.data,
		currDateObj = new Date(date.date[0]),
		currWeekDay = DAYS[currDateObj.getDay()],
		currDay = currDateObj.getDate(),
		currMonth = MONTHS[currDateObj.getMonth()];
	
	return (
		<div className="location">
			<p className="current-date">{ currWeekDay }, {currDay} {currMonth}</p>
			<h2>Weather in {cityName}, {country}</h2>
			<h3><img className="weather-image" src={`http://openweathermap.org/img/w/${icon[0]}.png`} alt=""/>{ Math.round(temperature[0]) } °C</h3>
		</div>
	);
};
