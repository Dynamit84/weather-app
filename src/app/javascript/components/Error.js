import React from 'react';

export const Error = (props) => {

    return <h1 className="error-msg">Unfortunately at the moment we {props.error}</h1>
};