import React from 'react';
import SearchResult from './SearchResult';
import * as actions from '../actions/actions';
import { connect } from 'react-redux';

class Header extends React.Component {

	constructor(props) {
		super(props);
        this.timerID = null;
        this.prevInputValue = null;
        this.currentInputValue = null;
	}

    checkToStartSearch(e) {

		this.currentInputValue = e.target.value;

		if(this.currentInputValue === '' || this.currentInputValue.length < 3) {
            clearTimeout(this.timerID);

            return;
        }

        if(e.keyCode === 13) {
            clearTimeout(this.timerID);
            this.sendSearchRequest();

            return;
        }

		if (this.prevInputValue !== this.currentInputValue) {
            clearTimeout(this.timerID);
            this.prevInputValue = this.currentInputValue;
            this.timerID = setTimeout(() => this.sendSearchRequest(), 1000);
		}
	}

    sendSearchRequest() {

		this.fetchCityList()
			.then(citiesList => this.props.searchCitiesResult(citiesList));
	}

    fetchCityList() {

        return fetch(`http://api.openweathermap.org/data/2.5/find?q=${this.currentInputValue}&type=accurate&appid=e81e98410b55421503581ee78e25d080`)
            .then(response => {
                if (response.ok) {
                    return Promise.resolve(response)
                } else {
                    return Promise.reject(new Error(response.statusText))
                }
            })
            .then(response => response.json())
            .then(data => this.extractData(data))
            .catch(error => error);
	}

    extractData(apiData) {

		return apiData.list.slice(0);
	}

	render() {
        return (
			<div className="header">
				<div className="header-flex">
					<img src="app/img/cloud.png" alt="icon" className="header-icon"/>
					<span>Current weather and forecasts in your city</span>
				</div>
				<div className="search-wrapper">
					<input type="search" className="search" placeholder="Type city name" onKeyUp={this.checkToStartSearch.bind(this)}/>
					<SearchResult searchResult={this.props.searchResult}/>
                    <div className="help"></div>
				</div>
			</div>
        )
	}
}

function mapStateToProps(state) {

    return {
        searchResult: state.searchResult
    };
}

export default connect(mapStateToProps, actions)(Header);
