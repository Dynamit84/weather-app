import React from 'react';

export const Tabs = (props) => {
	
	return (
		<ul className="tab">
			{
				props.data.map((elem, index) => {

					const { activeTab, onTabClick } = props,
						tabName = elem.dataName.toLowerCase(),
						active = activeTab === tabName ? 'active' : '';
					
					return <li
								key={index}
								className={`tab-item ${active}`}
								onClick={ () => onTabClick(tabName) }
							>{elem.dataName}</li>
				})
			}
		</ul>
	)
};