import React from 'react';
import { Error } from './Error';
import { GeneralInfo } from './GeneralInfo';
import { CurrentWeather } from './CurrentWeather';
import Charts from './Charts';

export const WeatherView = (props) => {
    const { errorMsg, data } = props;

    if(errorMsg) {

        return <Error error = { errorMsg }/>
    }

    return (
        <div>
            <GeneralInfo data = { data } />
            <CurrentWeather data = { data.weatherData } />
            <Charts data = { data }/>
        </div>
    )

};