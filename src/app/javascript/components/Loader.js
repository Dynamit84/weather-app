import React from 'react';

export const Loader = (props) => {
	const visible = props.isLoaderVisible ? ' visible' : '';

	return (
		<div className={`flex-wrapper${visible}`}>
			<div>
				<div className="sk-fading-circle">
					{[...Array(12)].map((elem, i) => {

						return <div key={i} className={`sk-circle sk-circle${i + 1}`}></div>
					})}
				</div>
				<p className="loader-msg">Please, wait...</p>
			</div>
		</div>
	);
};
