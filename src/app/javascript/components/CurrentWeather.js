import React from 'react';

export const CurrentWeather = (props) => {
	if(!props.data.speed) {

		return null;
	}

	let { speed , description, pressure, humidity } = props.data;
	
	return (
		<div className="current-weather">
			<ul className="list">
				<li className="item"><span className="bold">Pressure:</span> { Math.round(pressure[0]) } hpa</li>
				<li className="item"><span className="bold">Humidity:</span> { humidity[0] } %</li>
				<li className="item"><span className="bold">Wind speed:</span> { speed[0] } m/s</li>
				<li className="item">
					<span className="bold">Cloudiness:</span> <span className="first-letter">{ description[0] }</span>
				</li>
			</ul>
		</div>
	);
};
