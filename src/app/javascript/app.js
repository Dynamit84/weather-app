import React, { Component } from 'react';
import { Loader } from './components/Loader';
import Header from './components/Header';
import * as actions from './actions/actions';
import { connect } from 'react-redux';
import { WeatherView } from "./components/WeatherView";

class App extends Component {

	componentDidMount() {
        this.props.getCurrentPosition();
	}

	closeSearchResults(e) {
		if(e && e.keyCode !== 27 || this.props.searchResult === null) {

			return;
		}
        if(this.props.searchResult) {
            this.props.emptySearchResult();
        }
	}

	render() {
		const { isLoading, data, error } = this.props;
		
		return (
			<div className='root' onClick={() => this.closeSearchResults()} onKeyUp={(e) => this.closeSearchResults(e)}>
				<Loader isLoaderVisible = { isLoading } />
				<Header />
				<WeatherView errorMsg = { error } data = { data }/>
			</div>
		);
	}
}

function mapStateToProps(state) {
	
	return {
		data: state.data,
		isLoading: state.isLoading,
        searchResult: state.searchResult,
		error: state.error
	};
}

export default connect(mapStateToProps, actions)(App);
