let defaultState = {
	isLoading: false,
	activeChart: 'temperature',
	searchResult: null,
	error:'',
	data: {
		cityName: '',
		country: '',
		weatherData: {}
	}
};

export default function ( state = defaultState, action) {
	const { type, payload } = action;
	
	switch (type) {
		case 'FETCH_WEATHER_PENDING':
			
			return Object.assign({}, state, { isLoading: payload });
		case 'FETCH_WEATHER_FULFILLED':
			
			return Object.assign({}, state, { data: payload });

		case 'FETCH_WEATHER_REJECTED':

			return Object.assign({}, state, { error: payload });
		case 'CHANGE_ACTIVE_CHART':

			return Object.assign({}, state, { activeChart: payload });
		case 'FETCH_CITIES_FULFILLED':

			return Object.assign({}, state, { searchResult: payload });
		case 'EMPTY_SEARCH_RESULT':

			return Object.assign({}, state, { searchResult: null });
		default:

			return state;
	}
}

